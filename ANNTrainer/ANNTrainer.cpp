#include <iostream>
#include <ANN.h>
#include "MyANN.cpp"
#include "ANN.cpp"
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<vector<float>> input, output;
    vector<int> config;
    config = { 2, 10, 10, 1 };
    ANN::LoadData("../learning_file.txt", input, output);
    
    auto ann =  ANN::CreateNeuralNetwork(config);
	ann->MakeTrain(input, output, 100000);
    ann->Save("../data.txt"); 
	system("pause");
	return 0;
}